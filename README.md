# Lunar Lander

## Lunar Lander Environment
The problem consists of a 8-dimensional continuous state space and a discrete action space. There are four discrete actions available: do nothing, fire the left orientation engine, fire the main engine, fire the right orientation engine. The landing pad is always at coordinates (0,0). Coordinates consist of the first two numbers in the state vector. The total reward for moving from the top of the screen to landing pad ranges from 100 - 140 points varying on lander placement on the pad. If lander moves away from landing pad it is penalized the amount of reward that would be gained by moving towards the pad. An episode finishes if the lander crashes or comes to rest, receiving additional -100 or +100 points respectively. Each leg ground contact is worth +10 points. Firing main engine incurs a -0.3 point penalty for each occurrence. Landing outside of the landing pad is possible. Fuel is infinite, so, an agent could learn to fly and then land on its first attempt. The problem is considered solved when achieving a score of 200 points or higher on average over 100 consecutive runs.

## Agent Representation
As noted earlier, there are four actions available to your agent:
do nothing, fire the left orientation engine, fire the main engine, fire the right orientation engine
Additionally, at each time step, the state is provided to the agent as a 8-tuple:
(x, y, vx, vy, θ, vθ, left-leg, right-leg)
x and y are the x and y-coordinates of the lunar lander's position. vx and vy are the lunar lander's velocity components on the x and y axes. θ is the angle of the lunar lander. vθ is the angular velocity of the lander. Finally, left-leg and right-leg are binary values to indicate whether the left leg or right leg of the lunar lander is touching the ground. It should be noted, again, that you are working in a six dimensional continuous state space with the addition of two more discrete variables.

## Instructions to run the code ###

Please follow the steps in order to run the code:
	- open Project2.ipynb file in jupyter notebook
	- Run each cell sequentially.
	- Each cell a markdown comment explaining Outputs/functions that particular cell is implementing.
	- for ex. if you want to plot figure 1 of the report. run all the cells till figure 1 (including figure 1). It will generate figure 1 from 		the code.
	- The trained weights have been stored in "dqn_model_trained.h5" file. You can run these cells sequentially
		1) Importing Libraries
		2) Creating the Environment
		3)  Initializing Parameters
		4) Defining Keras Model
		5) Initializing prediction and Target models
		6) Function for choosing Actions
		7) Function to train target network
		8) Model Summary
		9) Testing the model
		10) Generating Test Scores
		11) Generate figure 2 of report

## Results

### Lander with Random Actions (Untrained)
![random](lunar_lander_random.gif)

### Trained Lunar Lander
![trained](lunar_lander_trained.gif)